<?php
/**
 * Created By: Rayane (r.mohamed@point-web.fr)
 * Date: 06/07/2022
 *
 * Update Symfony 5.4
 * Fichier de base pour faciliter les imports / synchronisations avec des fichiers CSV / EXCEL / JSON
 */

namespace Lk\ImportBundle\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class ImportBaseCommand
 */
abstract class ImportBaseCommand extends Command
{
    abstract protected function start();
    
    protected static $logFileName = null;
    protected static $customLogDir = null;
    
    /** @var  OutputInterface $output */
    protected $output;
    /** @var  InputInterface $output */
    protected $input;
    /** @var  EntityManager $em */
    protected $em;
    protected $kernel;
    private $log_file;
    private $start;
    private $importDir;
    private $logDir;
    
    /**
     * @param EntityManagerInterface $em
     * @param KernelInterface        $kernel
     */
    public function __construct(EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        
        $this->importDir = $kernel->getProjectDir();
        $this->logDir = $kernel->getLogDir();
        
        parent::__construct();
    }
    
    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->start = microtime(true);
        $this->output = $output;
        $this->input = $input;
        $this->initLog();
        $this->start();
        return $this->end();
    }
    
    /**
     * @param          $file
     * @param callable $caalback
     * @param          $dir
     * @return void
     */
    protected function importJSON($file, callable $caalback, $dir = null)
    {
        $inputFileName = ($dir !== null ? $dir : $this->importDir) . '/' . $file;
        $json = json_decode(file_get_contents($inputFileName));
        $datas = $json[2]->data;
        for ($x = 0; $x < count($datas); $x++) {
            $caalback($datas[$x]);
        }
    }
    
    /**
     * @param          $excel
     * @param callable $callback
     * @param          $dir
     * @param          $skipFirstLine
     * @param          $skipColumns
     * @param          $startRow
     * @param          $activeSheet
     * @return void
     */
    protected function importExcel($excel, callable $callback, $dir = null, $skipFirstLine = false, $skipColumns = false, $startRow = 0, $activeSheet = 1)
    {
        $objPHPExcel = $this->loadXLS($excel, $dir);
        $objWorksheet = $objPHPExcel->setActiveSheetIndex($activeSheet-1);
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumnNumber = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        $tab_attributes = array();
        for ($i = 0; $i < $highestColumnNumber; $i++) {
            $cell = $objWorksheet->getCellByColumnAndRow($i, 1);
            $val = $cell->getValue();
            $tab_attributes[$i] = $val;
        }
        if ($startRow != 0) {
            $nr = $startRow;
        } else {
            if ($skipFirstLine) $nr = 2; else
                $nr = 0;
        }
        for ($row = $nr; $row <= $highestRow; $row++) {
            for ($j = 0; $j < $highestColumnNumber; $j++) {
                $val = $objWorksheet->getCellByColumnAndRow($j, $row)->getFormattedValue();
                $callback($tab_attributes[$j], $val, $objWorksheet, $row, $j,($j==($highestColumnNumber-1)));
                if ($skipColumns) break;
            }
        }
        
        $objPHPExcel->disconnectWorksheets();
        $objPHPExcel->garbageCollect();
        unset($objPHPExcel);
    }
    
    /**
     * @param \PHPExcel_Worksheet $obj
     * @param                     $column
     * @param                     $row
     * @return mixed
     */
    protected function getValue(\PHPExcel_Worksheet $obj, $column, $row)
    {
        return $obj->getCellByColumnAndRow($column - 1, $row)->getFormattedValue();
    }
    
    /**
     * @param $name
     * @param $dir
     * @return void
     */
    private function loadXLS($name, $dir)
    {
        $inputFileName = ($dir !== null ? $dir : $this->importDir) . '/' . $name;
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (\Exception $e) {
            $this->write("Impossible d'ouvrir le fichier $name dans le dossier " . $dir === null ? "app" : $dir);
            exit();
        }
        
        return $objPHPExcel;
    }
    
    /**
     * @return void
     */
    private function initLog()
    {
        $this->log_file = fopen(static::$customLogDir ?: $this->logDir . "/".static::$logFileName.".txt", "a+");
        $now = (new \DateTime())->format('d/m/Y H:i:s');
        $this->write("Lancement import {$now}");
    }
    
    /**
     * @return int
     */
    private function end()
    {
        $end_time = microtime(true);
        $executionTime = round($end_time - $this->start, 2);
        $now = (new \DateTime())->format('d/m/Y H:i:s');
        $this->write("Fin de l'import : {$executionTime}s ($now)");
        fclose($this->log_file);
        
        return Command::SUCCESS;
    }
    
    /**
     * @return void
     */
    protected function clearLog()
    {
        file_put_contents(static::$customLogDir ?: $this->logDir . "/".static::$logFileName.".txt", "");
    }
    
    /**
     * @param $msg
     * @return void
     */
    protected function write($msg)
    {
        fwrite($this->log_file, $msg . "\r\n");
        $this->output->writeln($msg);
    }
    
    /**
     * @param $string
     * @return array|string|string[]
     * @deprecated It will be removed in 3.0
     */
    protected function forceUtf8($string)
    {
        //$string = utf8_decode($string);
        $string = str_ireplace('Ã¢', 'â', $string);
        $string = str_ireplace('Ã©', 'é', $string);
        $string = str_ireplace('à©', 'é', $string);
        $string = str_ireplace('í©', 'é', $string);
        $string = str_ireplace('Ã ', 'à', $string);
        $string = str_ireplace('Ã', 'à', $string);
        $string = str_ireplace('Ã¨', 'è', $string);
        $string = str_ireplace('Ã§', 'ç', $string);
        $string = str_ireplace('Â«', '«', $string);
        $string = str_ireplace('Â»', '»', $string);
        $string = str_ireplace("â€™", "'", $string);
        $string = str_ireplace('Ãª', 'ê', $string);
        $string = str_ireplace('àª', 'ê', $string);
        $string = str_ireplace('â‚¬', '€', $string);
        $string = str_ireplace('Ã´', 'ô', $string);
        $string = str_ireplace('Ã¤', 'ä', $string);
        $string = str_ireplace('Ã¹', 'ù', $string);
        $string = str_ireplace('Ã®', 'î', $string);
        $string = str_ireplace('à¨', 'è', $string);
        $string = str_ireplace('àª', 'ê', $string);
        $string = str_ireplace('Å“', 'œ', $string);
        $string = str_ireplace('à§', 'ç', $string);
        $string = str_ireplace('à»', 'û', $string);
        $string = str_ireplace('Ã»', 'û', $string);
        $string = str_ireplace('Ã¡', 'á', $string);
        $string = str_ireplace('à®', 'î', $string);
        $string = str_ireplace('à´', 'ô', $string);
        $string = str_ireplace('à‰', 'é', $string);
        $string = str_ireplace('à€', 'à', $string);
        $string = str_ireplace('Â', '', $string);
        $string = str_ireplace('Ã‰', 'É', $string);
        $string = str_ireplace('Ã¯', 'ï', $string);
        $string = str_ireplace('Ã³¯', 'ó', $string);
        $string = str_ireplace('à¯', 'ï', $string);
        $string = str_ireplace('À¯', 'ï', $string);
        
        return $string;
    }
    
    /**
     * @return EntityManager|EntityManagerInterface
     */
    public function getManager()
    {
        return $this->em;
    }
    
    /**
     * @return void
     */
    public function optimizeDoctrine() {
        $this->getManager()->getConnection()->getConfiguration()->setSQLLogger(null);
    }
}